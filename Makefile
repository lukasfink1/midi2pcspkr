CC = gcc
LD = gcc
RM = rm
EXECUTABLE = midi2pcspkr
LIBS = -lm -lasound
OBJS = ./main.o ./midi.o ./engine.o ./driver_console.o ./driver_evdev.o
CFLAGS = -O2 -Wall
LDFLAGS = 

.PHONY: all clean

all: $(EXECUTABLE)

clean:
	$(RM) $(EXECUTABLE) $(OBJS)

$(EXECUTABLE): $(OBJS)
	@echo 'Building target $@'
	$(LD) $(LDFLAGS) -o "$@" $(LIBS) $(OBJS)

%.o: src/%.c
	@echo 'Building file $<'
	$(CC) -c $(CFLAGS) -o "$@" "$<"

