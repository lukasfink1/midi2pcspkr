# midi2pcspkr

Let’s the builtin pc speaker (that one controlled by the pcspkr kernel module) be controlled by ALSA MIDI events.

## Build

`$ make all`

## Run

`usage: midi2pcspkr [-h] [-vv] [-c CHANNEL] [-b BENDRANGE] [midiport]...`

The flag -h is for help and -v/-vv is for verbose output. When -c is specified, it only listens to midi events from the given channel.
The -b flag allows control over the range of the pitch bend wheel (by default 2 semitones).
midiports are ALSA sequencer ports, to which the application subscribes at startup.

The program probably needs to be run as root to get access to the /dev/console ioctl KIOCSOUND.
It will only run wherever this ioctl and ALSA is available (probably only Linux).
