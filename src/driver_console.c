/*
 * Copyright (c) 2020 Lukas Fink
 * Released under the MIT license. Read 'LICENSE' for more information.
 */

#include "drivers.h"

#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/kd.h>
#include <linux/timex.h>
#include <math.h>

#ifndef PIT_TICK_RATE
#define PIT_TICK_RATE 1193182ul
#endif /* PIT_TICK_RATE */

static int fd = -1;

static int stop(void);

static int init(const char *path)
{
	if ((fd = open((path) ? path : "/dev/console", O_RDONLY)) < 0)
		return -1;
	if (stop())
	{
		close(fd);
		return -1;
	}

	return 0;
}

static int play(double freq)
{
	double count = PIT_TICK_RATE / freq;
	if (count > 0xffff)
		return stop();
	return ioctl(fd, KIOCSOUND, (uintptr_t)lround(count));
}

static int stop(void)
{
	return ioctl(fd, KIOCSOUND, (uintptr_t)0);
}

struct driver driver_console = { init, play, stop };
