/*
 * Copyright (c) 2020 Lukas Fink
 * Released under the MIT license. Read 'LICENSE' for more information.
 */

#include "drivers.h"

#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/input.h>
#include <math.h>

static int fd = -1;

static int init(const char *path)
{
	if ((fd = open((path) ? path : "/dev/input/by-path/platform-pcspkr-event-spkr", O_WRONLY)) < 0)
		return -1;
	if (ioctl(fd, EVIOCGSND(0), NULL))
	{
		close(fd);
		return -1;
	}

	return 0;
}

static int play(double freq)
{
	struct input_event ev;

	ev.type = EV_SND;
	ev.code = SND_TONE;
	ev.value = lround(freq);

	if (write(fd, &ev, sizeof(ev)) != sizeof(ev))
		return -1;

	return 0;
}

static int stop(void)
{
	struct input_event ev;

	ev.type = EV_SND;
	ev.code = SND_TONE;
	ev.value = 0;

	if (write(fd, &ev, sizeof(ev)) != sizeof(ev))
		return -1;

	return 0;
}

struct driver driver_evdev = { init, play, stop };
