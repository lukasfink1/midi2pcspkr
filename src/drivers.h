/*
 * Copyright (c) 2020 Lukas Fink
 * Released under the MIT license. Read 'LICENSE' for more information.
 */

#ifndef DRIVERS_H_
#define DRIVERS_H_

extern struct driver {
	int (*init)(const char *path);
	int (*play)(double freq);
	int (*stop)(void);
} driver_evdev, driver_console;

#endif /* DRIVERS_H_ */
