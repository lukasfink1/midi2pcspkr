/*
 * Copyright (c) 2019, 2020 Lukas Fink
 * Released under the MIT license. Read 'LICENSE' for more information.
 */

#include "engine.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "main.h"

double bend_range = 2. / 8192.;
struct driver drv = { NULL };

static unsigned char note_vec[24], *note_cur = NULL;
static int bend = 0;

static void rm_oldnote(unsigned char note);
static double freq(unsigned char note, int bend);

int note_on(unsigned char note)
{
	if (note_cur)
	{
		if (*note_cur == note)
			return 0;
		rm_oldnote(note);

		if (note_cur < &note_vec[sizeof note_vec - 1])
			note_cur++;
		else
			memmove(note_vec, note_vec + 1, sizeof note_vec - 1);
	}
	else
	{
		note_cur = note_vec;
	}
	*note_cur = note;
	return drv.play(freq(note, bend));
}

int note_off(unsigned char note)
{
	if (!note_cur) return 0;

	if (*note_cur == note)
	{
		if (note_cur > note_vec)
		{
			note_cur--;
			return drv.play(freq(*note_cur, bend));
		}
		else
		{
			note_cur = NULL;
			return drv.stop();
		}
	}

	rm_oldnote(note);
	return 0;
}

int all_notes_off(void)
{
	note_cur = NULL;
	return drv.stop();
}

int set_bend(int _bend)
{
	if (bend != _bend && (_bend == 0 || abs(bend - _bend) >> 8)) /* Changing the pitch too often sounds terrible */
	{
		if (verbose >= 2)
			puts("changed bend value");
		bend = _bend;
		if (note_cur)
			return drv.play(freq(*note_cur, bend));
	}
	return 0;
}

static void rm_oldnote(unsigned char note)
{
	for (unsigned char *note_p = note_vec; note_p < note_cur; note_p++)
	{
		if (*note_p == note)
		{
			memmove(note_p, note_p + 1, note_cur - note_p);
			note_cur--;
			return;
		}
	}
}

static double freq(unsigned char note, int bend)
{
	return 440. * exp(M_LN2 / 12. * (note - 69 + bend * bend_range));
}
