/*
 * Copyright (c) 2019, 2020 Lukas Fink
 * Released under the MIT license. Read 'LICENSE' for more information.
 */

#ifndef ENGINE_H_
#define ENGINE_H_

#include "drivers.h"

extern double bend_range;
extern struct driver drv;

int note_on(unsigned char note);
int note_off(unsigned char note);
int set_bend(int bend);
int all_notes_off(void);

#endif /* ENGINE_H_ */
