/*
 * Copyright (c) 2019, 2020 Lukas Fink
 * Released under the MIT license. Read 'LICENSE' for more information.
 */

#include "main.h"

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>

#include "midi.h"
#include "engine.h"
#include "drivers.h"

extern char *optarg;
extern int optind;

int verbose = 0;

static char *devpath = NULL;

static void argparse(int argc, char *argv[]);

static void usage(const char *name)
{	/* usage: %s [-h] [-vv] [-f FILE] [-d DRIVER] [-c CHANNEL] [-b BENDRANGE] [midiport]… */
	printf("usage: %s [option]... [midiport]...\n\n"
		"Allows the PC speaker to be controlled via ALSA MIDI.\n\n"
		"  midiport               subscribe to midi port\n\n"
		"  -b BENDRANGE           set the range of the pitch bend wheel to +/- BENDRANGE\n"
		"                         semitones; negative values invert the pitch bend wheel\n"
		"                         direction (default 2)\n"
		"  -c CHANNEL             only parse events on CHANNEL (0-15)\n"
		"  -d DRIVER              force usage of DRIVER for sound output (evdev, console)\n"
		"  -f FILE                use FILE as device file for sound output\n"
		"  -q                     activate quiet mode; disables status messages\n"
		"  -v                     increase verbosity; use twice to print pitchbend events\n"
		"  -h                     print this help\n", name);
	exit(0);
}

int main(int argc, char *argv[])
{
	int err;

	argparse(argc, argv);

	if (drv.init)
	{
		if (drv.init(devpath) < 0)
		{
			perror(argv[0]);
			return 1;
		}
	}
	else
	{
		if (!driver_evdev.init(devpath))
		{
			if (verbose >= 0)
				puts("using evdev driver");
			drv = driver_evdev;
		}
		else if (!driver_console.init(devpath))
		{
			if (verbose >= 0)
				puts("using console driver");
			drv = driver_console;
		}
		else
		{
			fprintf(stderr, "%s: all drivers failed to initialize.\n", argv[0]);
			return 1;
		}
	}

	if ((err = seq_init("PC speaker")) < 0)
		goto alsaerr;

	for (char **p = &argv[optind]; *p; p++)
		if ((err = seq_subscribe(*p)) < 0)
			goto alsaerr;

	mainloop(argv[0]);

	return 0;

alsaerr:
	sperror(argv[0], err);
	return 1;
}

static void argparse(int argc, char *argv[])
{
	char c, *end = NULL;
	unsigned long num;

	while ((c = getopt(argc, argv, "hvqc:b:d:f:")) != -1)
	{
		switch (c)
		{
		case 'h':
			usage(argv[0]);
			break;
		case 'v':
			if (verbose < INT_MAX)
				verbose++;
			break;
		case 'q':
			verbose = -1;
			break;
		case 'c':
			num = strtoul(optarg, &end, 0);
			if (*end || num > 15)
			{
				fprintf(stderr, "%s: channel number invalid: %s\n", argv[0], optarg);
				exit(2);
			}
			channel = num;
			break;
		case 'b':
			bend_range = strtod(optarg, &end) / 8192.;
			if (*end)
			{
				fprintf(stderr, "%s: bend range invalid: %s\n", argv[0], optarg);
				exit(2);
			}
			break;
		case 'd':
			if (!strcmp(optarg, "evdev"))
			{
				drv = driver_evdev;
			}
			else if (!strcmp(optarg, "console"))
			{
				drv = driver_console;
			}
			else
			{
				fprintf(stderr, "%s: no such driver: %s\n", argv[0], optarg);
				exit(2);
			}
			break;
		case 'f':
			devpath = optarg;
			break;
		case '?':
			fprintf(stderr, "Try '%s -h' for more information.\n", argv[0]);
			exit(2);
		}
	}
}
