/*
 * Copyright (c) 2019 Lukas Fink
 * Released under the MIT license. Read 'LICENSE' for more information.
 */

#include "midi.h"

#include <stdio.h>
#include <errno.h>
#include <alloca.h>
#include <alsa/asoundlib.h>

#include "main.h"
#include "engine.h"

char channel = -1;

static snd_seq_t *seqh = NULL;
static int port = -1;

void sperror(const char *name, int err)
{
	fprintf(stderr, "%s: %s\n", name, snd_strerror(err));
}

int seq_init(const char *seqname)
{
	snd_seq_client_info_t *info;
	int err;

	if ((err = snd_seq_open(&seqh, "default", SND_SEQ_OPEN_INPUT, 0)) < 0)
		return err;
	snd_seq_client_info_alloca(&info);
	if ((err = snd_seq_get_client_info(seqh, info)) < 0)
		goto openerr;
	snd_seq_client_info_set_name(info, seqname);
	snd_seq_client_info_event_filter_add(info, SND_SEQ_EVENT_NOTEON);
	snd_seq_client_info_event_filter_add(info, SND_SEQ_EVENT_NOTEOFF);
	snd_seq_client_info_event_filter_add(info, SND_SEQ_EVENT_PITCHBEND);
	snd_seq_client_info_event_filter_add(info, SND_SEQ_EVENT_CONTROLLER);

	if ((err = snd_seq_set_client_info(seqh, info)) < 0 ||
		(err = port = snd_seq_create_simple_port(seqh, seqname,
			SND_SEQ_PORT_CAP_WRITE | SND_SEQ_PORT_CAP_SUBS_WRITE,
			SND_SEQ_PORT_TYPE_MIDI_GENERIC | SND_SEQ_PORT_TYPE_HARDWARE |
			SND_SEQ_PORT_TYPE_SYNTHESIZER)) < 0)
		goto openerr;

	if (verbose >= 0)
		printf("created midi port %d:%d\n", snd_seq_client_id(seqh), port);

	return 0;
openerr:
	snd_seq_close(seqh);
	return err;
}

int seq_subscribe(const char *str)
{
	snd_seq_addr_t addr;
	int err;

	if ((err = snd_seq_parse_address(seqh, &addr, str)) < 0 ||
		(err = snd_seq_connect_from(seqh, port, addr.client, addr.port)) < 0)
		return err;
	if (verbose >= 0)
		printf("subscribed to sender port %d:%d\n", addr.client, addr.port);
	return 0;
}

void mainloop(const char *name)
{
	snd_seq_event_t *ev = NULL;
	int err;

	while (1)
	{
		if ((err = snd_seq_event_input(seqh, &ev)) < 0)
		{
			sperror(name, err);
			if (err == -ENOSPC &&
				all_notes_off() < 0)
				perror(name);
			continue;
		}

		if (channel >= 0 && ev->data.control.channel != channel) continue;

		switch (ev->type)
		{
		case SND_SEQ_EVENT_CONTROLLER:
			if (ev->data.control.param != 123)
				break;
			if (verbose >= 1)
				puts("received all notes off event");
			if (all_notes_off() < 0)
				perror(name);
			break;
		case SND_SEQ_EVENT_PITCHBEND:
			if (verbose >= 2)
				printf("received pitchbend event, value=%d\n", ev->data.control.value);
			if (set_bend(ev->data.control.value) < 0)
				perror(name);
			break;
		case SND_SEQ_EVENT_NOTEON:
			if (verbose >= 1)
				printf("received note on event, note=%d, velocity=%d\n",
					ev->data.note.note, ev->data.note.velocity);
			if (ev->data.note.velocity == 0) goto noteoff_skip;
			if (note_on(ev->data.note.note) < 0)
				perror(name);
			break;
		case SND_SEQ_EVENT_NOTEOFF:
			if (verbose >= 1)
				printf("received note off event, note=%d\n", ev->data.note.note);
		noteoff_skip:
			if (note_off(ev->data.note.note) < 0)
				perror(name);
			break;
		}
	}
}

