/*
 * Copyright (c) 2019 Lukas Fink
 * Released under the MIT license. Read 'LICENSE' for more information.
 */

#ifndef MIDI_H_
#define MIDI_H_

extern char channel;

int seq_init(const char *seqname);
void mainloop(const char *name);
void sperror(const char *name, int err);
int seq_subscribe(const char *str);

#endif /* MIDI_H_ */
